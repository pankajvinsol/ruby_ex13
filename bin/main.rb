require_relative '../lib/fixnum.rb'

print 'Enter a Number : '
number = gets.to_i
begin
puts "Factorial of #{ number } is #{ number.factorial }"
rescue RangeError => details
  p details
end
