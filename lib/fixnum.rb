class Fixnum
  def factorial
    raise(RangeError, "Can't Calculate Factorial for Negative Number") if self < 0
    (1..self).inject(1, :*)
  end
end

